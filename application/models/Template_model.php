<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Template_model extends MY_Model {

	public function data_draft(){
		$query = "SELECT * FROM $this->t_pelaporan
				 WHERE int_status = '9'
				 ORDER BY dt_tanggal_pelaporan DESC LIMIT 5";
		$data = $this->db->query($query);
        return $data->result_array();
	}
	public function data_tanggapan(){
		$query = "SELECT tp.`txt_judul_pelaporan`, tp.`var_kode_pelaporan`, tt.`dt_tanggal_tanggapan` FROM $this->t_tanggapan tt
					LEFT JOIN $this->t_pelaporan tp ON tt.`int_id_pelaporan` = tp.`int_id_pelaporan`
					ORDER BY dt_tanggal_tanggapan DESC LIMIT 5";
		$data = $this->db->query($query);
        return $data->result_array();
	}


}