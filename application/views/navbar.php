<header>
    <div class="header-top sticky bg-white d-none d-lg-block">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <!-- header top navigation start -->
                    <div class="header-top-navigation">
                        <nav>
                            <ul>
                                <li class="active"><a href="<?=base_url()?>">beranda</a></li>
                                <li class="msg-trigger"><a class="msg-trigger-btn" href="#laporan">laporan</a>
                                    <div class="message-dropdown" id="laporan">
                                        <ul class="dropdown-msg-list">
                                            <li class="msg-list-item d-flex justify-content-between">
                                                <h6 class=""><a href="#">Pengaduan</a></h6>
                                            </li>
                                            <li class="msg-list-item d-flex justify-content-between">
                                                <h6 class=""><a href="#">Pertanyaan</a></h6>
                                            </li>
                                            <li class="msg-list-item d-flex justify-content-between">
                                                <h6 class=""><a href="#">Insiden</a></h6>
                                            </li>
                                            <li class="msg-list-item d-flex justify-content-between">
                                                <h6 class=""><a href="#">Kritik</a></h6>
                                            </li>
                                            <li class="msg-list-item d-flex justify-content-between">
                                                <h6 class=""><a href="#">Saran</a></h6>
                                            </li>
                                            <li class="msg-list-item d-flex justify-content-between">
                                                <h6 class=""><a href="#">Info</a></h6>
                                            </li>
                                            <li class="msg-list-item d-flex justify-content-between">
                                                <h6 class=""><a href="#">Lainnya</a></h6>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="msg-trigger"><a class="msg-trigger-btn" href="#opd">OPD</a>
                                    <div class="message-dropdown" id="opd">
                                        <ul class="dropdown-msg-list">
                                            <li class="msg-list-item d-flex justify-content-between">
                                                <h6 class=""><a href="<?=base_url()?>opd">Daftar OPD</a></h6>
                                            </li>
                                            <li class="msg-list-item d-flex justify-content-between">
                                                <h6 class=""><a href="<?=base_url()?>opd/rekapitulasi">Rekapitulasi</a></h6>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <!-- header top navigation start -->
                </div>

                <div class="col-md-6">
                    <div class="header-top-right d-flex align-items-center justify-content-end">
                        <!-- header top search start -->
                        <div class="header-top-search">
                            <form class="top-search-box">
                                <input type="text" placeholder="Search" class="top-search-field">
                                <button class="top-search-btn"><i class="fas fa-search"></i></button>
                            </form>
                        </div>
                        <!-- header top search end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- header area end -->
<!-- header area start -->
<header>
    <div class="mobile-header-wrapper sticky d-block d-lg-none">
        <div class="mobile-header position-relative ">
            <div class="mobile-menu w-100">
                <ul>
                    <li>
                        <a href="<?=base_url()?>" class="notification"><i class="fas fa-th-large"></i>
                        </a>
                    </li>
                    <li>
                        <button class="notification laporan-trigger"><i class="fas fa-list-alt"></i>
                        </button>
                        <ul class="laporan-list">
                            <li>
                                <a href="profile.html"><div class="submenu-mobile">
                                    <div class="frnd-content">
                                        <h6 class="author">Pengaduan</h6>
                                    </div>
									</div></a>
                            </li>
                            <li>
                                <div class="submenu-mobile">
                                    <div class="frnd-content">
                                        <h6 class="author"><a href="profile.html">Pertanyaan</a></h6>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="submenu-mobile">
                                    <div class="frnd-content">
                                        <h6 class="author"><a href="profile.html">Insiden</a></h6>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="submenu-mobile">
                                    <div class="frnd-content">
                                        <h6 class="author"><a href="profile.html">Kritik</a></h6>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="submenu-mobile">
                                    <div class="frnd-content">
                                        <h6 class="author"><a href="profile.html">Saran</a></h6>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="submenu-mobile">
                                    <div class="frnd-content">
                                        <h6 class="author"><a href="profile.html">Info</a></h6>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="submenu-mobile">
                                    <div class="frnd-content">
                                        <h6 class="author"><a href="profile.html">Lainnya</a></h6>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <button class="notification opd-trigger"><i class="far fa-building"></i>
                        </button>
                        <ul class="opd-menu-list">
                            <li>
                                <div class="submenu-mobile">
                                    <div class="frnd-content">
                                        <h6 class="author"><a href="<?=base_url()?>opd">Daftar OPD</a></h6>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="submenu-mobile">
                                    <div class="frnd-content">
                                        <h6 class="author"><a href="<?=base_url()?>rekapitulasi">Rekapitulasi</a></h6>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <button class="search-trigger">
                            <i class="search-icon fas fa-search"></i>
                            <i class="close-icon fas fa-times"></i>
                        </button>
                        <div class="mob-search-box">
                            <form class="mob-search-inner">
                                <input type="text" placeholder="Search Here" class="mob-search-field">
                                <button class="mob-search-btn"><i class="fas fa-search"></i></button>
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
         </div>
    </div>
</header>
<!-- header area end -->
