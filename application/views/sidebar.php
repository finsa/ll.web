<div class="col-lg-3 order-3">
	<aside class="widget-area">
		<!-- widget single item start -->
		<div class="card widget-item">
			<h4 class="widget-title">Tanggapan Terkini</h4>
			<div class="widget-body">
				<ul class="like-page-list-wrapper">
					<?=$tanggapan?>
				</ul>
			</div>
		</div>
		<div class="card widget-item">
			<h4 class="widget-title">Terbaru di Facebook</h4>
			<div class="widget-body">
				<ul class="like-page-list-wrapper">
					<?=$draft?>
				</ul>
			</div>
		</div>
		<!-- widget single item end -->

	</aside>
