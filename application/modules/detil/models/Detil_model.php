<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Detil_model extends MY_Model {

	function detil_pelaporan($var_kode_pelaporan){
		$query = "SELECT tp.*, tpi.txt_dir, tpi.int_source, ms.`txt_sumber`, mst.txt_status, mst.var_class stat_class, mk.txt_kategori, mk.var_class kat_class, mk.var_color
                    FROM $this->t_pelaporan tp
                    LEFT JOIN $this->t_pelaporan_img tpi ON tp.int_id_pelaporan = tpi.int_id_pelaporan
                    LEFT JOIN $this->m_sumber ms ON tp.int_id_sumber = ms.int_id_sumber
                    LEFT JOIN $this->m_kategori mk ON tp.int_id_kategori = mk.int_id_kategori
                    LEFT JOIN $this->m_status mst ON tp.int_status = mst.int_status
                    WHERE var_kode_pelaporan = '".$var_kode_pelaporan."'";
		$data = $this->db->query($query);
        return $data->result_array();
	}
	public function ret_tujuan_pelaporan($int_id_pelaporan){
		$this->db->select("ml.txt_deskripsi, ml.int_id_lembaga");
		$this->db->from("{$this->t_pelaporan_lembaga} tpl");
		$this->db->join("{$this->m_lembaga} ml", "tpl.int_id_lembaga = ml.int_id_lembaga", "left");
		$this->db->where("tpl.int_id_pelaporan" , $int_id_pelaporan);
		$data = $this->db->get();
		return $data->result_array();
	}

	public function ret_img_pelaporan($int_id_pelaporan){
		$this->db->select("int_id_pelaporan_img, txt_dir");
		$this->db->from("$this->t_pelaporan_img");
		$this->db->where("int_id_pelaporan" , $int_id_pelaporan);
		$data = $this->db->get();
        return $data->result_array();
	}
	function ret_tanggapan_id_pelaporan($int_id_pelaporan){
		$this->db->select("tt.*, CONCAT(mu.txt_nama_depan, ' ', mu.txt_nama_belakang) as txt_user, ms.*");
		$this->db->from("{$this->t_tanggapan} tt");
		$this->db->join("{$this->m_status} ms", "tt.int_status = ms.int_status", "left");
		$this->db->join("{$this->s_user} mu", "tt.created_by = mu.int_id_user", "left");
		$this->db->where("int_id_pelaporan", $int_id_pelaporan);
		$data = $this->db->get();
        return $data->result_array();
	}
	function ret_img_tanggapan($int_id_tanggapan){
		$this->db->select("int_id_tanggapan_img, txt_dir");
		$this->db->from("{$this->t_tanggapan_img}");
		$this->db->where("int_id_tanggapan", $int_id_tanggapan);
		$data = $this->db->get();
        return $data->result_array();
	}

}