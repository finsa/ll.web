<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Grab_model extends MY_Model {

	public function get_data(){
		return $this->db->query("SELECT * FROM {$this->m_facebook} WHERE int_id_facebook = 0")->row();
	}
	 
    public function update($upd){
        $this->db->where('int_id_facebook', 0)
                ->update($this->m_facebook, $upd);
    }

    public function check_fb($var_kode_pelaporan){
		$this->db->from("$this->t_pelaporan");
		$this->db->like('var_kode_pelaporan', $var_kode_pelaporan);
		$ret = $this->db->count_all_results();
		return $ret;
	}

	public function create($ins, $lampiran = []){
		$lembaga_list = $ins['int_id_lembaga'];
		$ins['var_kode_pelaporan'] = $ins['var_kode_pelaporan'].date("ymdHis");
		//$ins['created_by'] = $this->session->userdata['user_id'];
		$this->db->trans_begin();
		$ins = $this->clearFormInsert($ins, ['int_id_lembaga', 'lampiran', 'is_cover', 'txt_desc', 'img_select', 'cms_token', 'cdImg', 'int_source', 'txt_prefix']);

		$this->db->insert($this->t_pelaporan, $ins);
		$int_id_pelaporan = $this->db->insert_id();
		$this->setQueryLog('ins_pelaporan');

		if(!empty($lembaga_list)){
		    $inslembaga = [];
		    foreach($lembaga_list as $int_id_lembaga){
		        $inslembaga[] = ['int_id_pelaporan' => $int_id_pelaporan, 'int_id_lembaga' => $int_id_lembaga];
            }
		    $this->db->insert_batch($this->t_pelaporan_lembaga, $inslembaga);
        }

		if(!empty($lampiran)){
			//print_r($lampiran);
			$lampiran_insert = [];
			foreach($lampiran as $gal){
				$lampiran_insert[] = ['int_id_pelaporan' => $int_id_pelaporan,
									'txt_dir' => $this->db->escape($gal['dir'].$gal['file_name']),
									'txt_desc' => $this->db->escape($gal['txt_desc']),
									'is_cover' => $this->db->escape($gal['is_cover']),
									'txt_size' => $this->db->escape(round($gal['file_size'],2)),
									'txt_type' => $this->db->escape($gal['file_type']),
									'int_source' => $this->db->escape($gal['int_source'])];
			}
			$this->db->insert_batch($this->t_pelaporan_img, $lampiran_insert, false);
			$this->setQueryLog('ins_lampiran');
		}

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}	

}