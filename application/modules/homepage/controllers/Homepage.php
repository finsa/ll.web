<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homepage extends MX_Controller {
    var $limit = "15";

	function __construct(){
        parent::__construct();
		
		$this->kodeMenu = 'APP-HOME';
		//$this->authCheck();
		
		$this->load->model('homepage_model', 'model');
    }
	
	public function index($page = 1){
        $start = $this->limit * ($page - 1);
        $rekap_progress = $this->model->rekap_progress();
		$rekap_top_opd = $this->model->rekap_top_opd();
        $count_data_pelaporan = $this->model->count_data_pelaporan();
        $data_pelaporan = $this->model->data_pelaporan($start,$this->limit);
        //print_r($data_pelaporan);

        foreach ($rekap_progress as $pro){
			$data_progress[$pro['int_status']] = $pro['jumlah'];
		}

        $no = 1;
		$top_opd = '';
		foreach ($rekap_top_opd as $opd){
			$top_opd .= '<li class="unorder-list">
								<div class="top-count">
									<span class="badge badge-pill badge-secondary">'.$no.'</span>
								</div>
								<div class="unorder-list-info">
									<h3 class="list-title"><a href="'.base_url().'opd/'.$opd['int_id_lembaga'].'">'.$opd['txt_nama_lembaga'].'</a></h3>
									<p class="list-subtitle">'.$opd['jumlah'].' Laporan</p>
								</div>
							</li>';
		$no++;
		}

        $list_pelaporan = '';
        foreach ($data_pelaporan as $row){	
            $txt_img = '';//THEME_URL.'images/laporlumajang.jpg';
			if($row['txt_dir'] != ''){
                if($row['int_source']==1){
                    $img_url = $row['txt_dir'];
                }else{
                    $img_url = cdn_url().$row['txt_dir'];
                }
				$txt_img = '<div class="post-thumb-gallery">
							<figure class="post-thumb img-popup">
								<a href="'.$img_url.'">
									<img src="'.$img_url.'" alt="'.$row['txt_judul_pelaporan'].'">
								</a>
							</figure>
						</div>';
			}
			
			$tujuan = '-';
			$tujuan_pelaporan = $this->model->ret_tujuan_pelaporan($row['int_id_pelaporan']);
			if(!empty($tujuan_pelaporan)){
				$tujuan = '<i class="far fa-building" style="margin-right:5px;"> </i>';
				foreach($tujuan_pelaporan as $rowTujuan){
					$tujuan .= '<a href="'.base_url().'opd/'.$rowTujuan['int_id_lembaga'].'">'.$rowTujuan['txt_deskripsi'].'</a>,';
				}
            }
            
            $nama_pelapor = isset($row['txt_nama_pelapor'])? $row['txt_nama_pelapor'] : 'Member Grup Facebook';
            $var_color = isset($row['var_color'])? $row['var_color'] : '#6c757d';
            $kat_class = isset($row['kat_class'])? $row['kat_class'] : 'fas fa-folder';

            $list_pelaporan .= '<div class="card status-'.$row['int_id_kategori'].'">
						<div class="post-title d-flex align-items-center">
							<!-- profile picture end -->
							<div class="profile-thumb">
								<figure class="profile-thumb-middle lap-jenis-'.$row['int_id_kategori'].'" style="background:'.$var_color.'">
								<i class="'.$kat_class.'"></i>
								</figure>
							</div>
							<!-- profile picture end -->
				
							<div class="posted-author">
								<h6 class="author">'.$nama_pelapor.'</h6>
								<span class="post-time">'.date('d F Y H:i:s', strtotime($row['dt_tanggal_pelaporan'])).'</span>
								
							</div>
							<div class="post-settings-bar">
								<a href="'.$row['txt_url'].'" target="_blank" class="badge badge-primary">
									<i class="fab fa-facebook-f"> </i>
								</a>
								<a href="'.base_url().'detil/'.$row['var_kode_pelaporan'].'" class="badge badge-success">
									<i class="fas fa-info"> </i>
								</a>
							</div>
						</div>
						<!-- post title start -->
						<div class="post-content">
							<p class="post-desc">
							<b><a href="'.base_url().'detil/'.$row['var_kode_pelaporan'].'" style="color:#000">'.ucwords($row['txt_judul_pelaporan']).'</a></b>
							<span class="badge badge-pill badge-'.$row['stat_class'].'">'.$row['txt_status'].'</span><br>
							'.strip_tags($row['txt_detil_pelaporan']).'
							</p>
							'.$txt_img.'
							<div class="post-meta">
								<span class="post-meta-like">
									'.substr($tujuan, 0, -1).'
								</span>
							</div>
						</div>
					</div>';
        }
        
        $data['progress']  = $data_progress;
        $data['top_opd']   = $top_opd;//$this->model->get_posts(2,6);
        $data['pelaporan']   = $list_pelaporan;
		$data["pagination"] = $this->_build_pagination($page,$count_data_pelaporan,$this->limit,base_url()."index/");
		$this->render_view('index', $data, false, 'template');
	}
}
