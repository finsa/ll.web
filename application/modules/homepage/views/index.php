<div class="col-lg-3 order-2 order-lg-1">
	<aside class="widget-area">
		<!-- widget single item start -->
		<div class="card card-profile widget-item p-0">
			<div class="profile-banner">
				<figure class="profile-banner-small">
					<img src="<?=base_url()?>thm/images/laporlumajang.jpg" alt="">
				</figure>
				<div class="profile-desc text-center">
					<h6 class="author"><a href="profile.html">Lapor Lumajang</a></h6>
					<p>Kanal Pelaporan resmi Kabupaten Lumajang melalui media sosial Facebook</p>
				</div>
			</div>
		</div>
		<!-- widget single item start -->

		<div class="card widget-item">
			<h4 class="widget-title">Rekapitulasi</h4>
			<div class="widget-body">
				<ul class="like-page-list-wrapper">
					<li class="unorder-list">
						<!-- profile picture end -->
						<div class="rek-count">
							<span class="badge badge-pill badge-danger"><?=isset($progress[0])? $progress[0] : '0';?></span>
						</div>
						<!-- profile picture end -->
						<div class="unorder-list-info">
							<p class="list-subtitle"><a href="#">laporan</a></p>
							<h3 class="list-title"><a href="#">Belum Ditanggapi</a></h3>
						</div>
					</li>
					<li class="unorder-list">
						<!-- profile picture end -->
						<div class="rek-count">
							<span class="badge badge-pill badge-warning"><?=isset($progress[1])? $progress[1] : '0';?></span>
						</div>
						<!-- profile picture end -->
						<div class="unorder-list-info">
							<p class="list-subtitle"><a href="#">laporan</a></p>
							<h3 class="list-title"><a href="#">Dalam Penanganan</a></h3>
						</div>
					</li>
					<li class="unorder-list">
						<!-- profile picture end -->
						<div class="rek-count">
							<span class="badge badge-pill badge-primary"><?=isset($progress[2])? $progress[2] : '0';?></span>
						</div>
						<!-- profile picture end -->
						<div class="unorder-list-info">
							<p class="list-subtitle"><a href="#">laporan</a></p>
							<h3 class="list-title"><a href="#">Dalam Perencanaan</a></h3>
						</div>
					</li>
					<li class="unorder-list">
						<!-- profile picture end -->
						<div class="rek-count">
							<span class="badge badge-pill badge-success"><?=isset($progress[3])? $progress[3] : '0';?></span>
						</div>
						<!-- profile picture end -->
						<div class="unorder-list-info">
							<p class="list-subtitle"><a href="#">laporan</a></p>
							<h3 class="list-title"><a href="#">Terselesaikan</a></h3>
						</div>
					</li>
				</ul>
			</div>
			<h6 class="widget-footer" style="border-top: 1px solid #1c8567;margin-top: 30px;font-size: 12px;color: #1c8567;">
			*) 30 hari terakhir</h6>
		</div>

		<div class="card widget-item">
			<h4 class="widget-title">Top OPD</h4>
			<div class="widget-body">
				<ul class="like-page-list-wrapper">
					<?=$top_opd?>
				</ul>
			</div>
			<h6 class="widget-footer" style="border-top: 1px solid #1c8567;margin-top: 30px;font-size: 12px;color: #1c8567;">
			*) 30 hari terakhir</h6>
		</div>
		<!-- widget single item end -->
	</aside>
</div>


<div class="col-lg-6 order-1 order-lg-2">
	<!-- post status start -->
	<?=$pelaporan;?>
	<?php echo $pagination;?>
	<!-- post status end -->
</div>