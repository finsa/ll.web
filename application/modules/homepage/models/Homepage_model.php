<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Homepage_model extends MY_Model {

    public function rekap_progress(){
		$date = date_create(date('Y-m-d'));
		date_sub($date,date_interval_create_from_date_string("30 days"));
		$dt_start = date_format($date,"Y-m-d H:i:s");
		$query = "	SELECT int_status, COUNT(int_id_pelaporan) AS jumlah
					FROM $this->t_pelaporan 
					WHERE dt_tanggal_pelaporan BETWEEN '".$dt_start."' AND '".date("Y-m-d H:i:s")."'
					GROUP BY int_status";
		$data = $this->db->query($query);
        return $data->result_array();
    }
    
    public function rekap_top_opd(){
		$date = date_create(date('Y-m-d'));
		date_sub($date, date_interval_create_from_date_string("30 days"));
		$dtStart = date_format($date,"Y-m-d H:i:s");
		$query = "SELECT * FROM (
                    SELECT COUNT(tpl.int_id_lembaga) AS jumlah, ml.txt_nama_lembaga, ml.int_id_lembaga
                    FROM $this->m_lembaga ml
                    LEFT JOIN $this->t_pelaporan_lembaga tpl ON tpl.`int_id_lembaga` = ml.`int_id_lembaga`
                    LEFT JOIN $this->t_pelaporan tp ON tpl.`int_id_pelaporan` = tp.`int_id_pelaporan`
                    WHERE tp.`dt_tanggal_pelaporan` BETWEEN '".$dtStart."' AND '".date("Y-m-d H:i:s")."'
                    GROUP BY ml.`int_id_lembaga`) sc
				ORDER BY sc.jumlah DESC, sc.txt_nama_lembaga ASC LIMIT 10";
		$data = $this->db->query($query);
        return $data->result_array();
	}

    public function count_data_pelaporan(){
		$this->db->select("int_id_pelaporan");
		$this->db->from($this->t_pelaporan);
        $data = $this->db->count_all_results();
		return $data;
	}

    public function data_pelaporan($start,$limit){
		$query = "SELECT tp.*, tpi.txt_dir, tpi.int_source, ms.`txt_sumber`, mst.txt_status, mst.var_class stat_class, mk.var_class kat_class, mk.var_color
                    FROM $this->t_pelaporan tp
                    LEFT JOIN $this->t_pelaporan_img tpi ON tp.int_id_pelaporan = tpi.int_id_pelaporan
                    LEFT JOIN $this->m_sumber ms ON tp.int_id_sumber = ms.int_id_sumber
                    LEFT JOIN $this->m_kategori mk ON tp.int_id_kategori = mk.int_id_kategori
                    LEFT JOIN $this->m_status mst ON tp.int_status = mst.int_status
                    WHERE is_active = '1'
                    ORDER BY tp.dt_tanggal_pelaporan DESC, tp.dt_created DESC, tpi.int_source ASC LIMIT ".$limit." OFFSET ".$start;
		$data = $this->db->query($query);
        return $data->result_array();
    }
    
	public function ret_tujuan_pelaporan($int_id_pelaporan){
		$this->db->select("ml.txt_deskripsi, ml.int_id_lembaga");
		$this->db->from("{$this->t_pelaporan_lembaga} tpl");
		$this->db->join("{$this->m_lembaga} ml", "tpl.int_id_lembaga = ml.int_id_lembaga", "left");
		$this->db->where("tpl.int_id_pelaporan" , $int_id_pelaporan);
		$data = $this->db->get();
		return $data->result_array();
	}
}