<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Opd_model extends MY_Model {

	function get_data_lembaga(){
		$this->db->select("*");
		$this->db->from("$this->m_lembaga");
		$this->db->order_by("int_order" , "ASC");
		$data = $this->db->get();
        return $data->result_array();
	}

	public function data_pelaporans($int_id_lembaga,$start,$limit){
		$query = "SELECT * FROM(
					SELECT A.`int_id_pelaporan`, A.`var_kode_pelaporan`, A.`txtNamaPelapor`, A.`txtJudulPelaporan`, A.`txtDetilPelaporan`, A.`dtTanggalPelaporan`, A.`txtUrl`, A.dtCreated, A.`intProgress`, A.`intJenis`, B.`txtImg`
					FROM pelaporan AS A
					LEFT JOIN pelaporanimg AS B ON A.`int_id_pelaporan` = B.`int_id_pelaporan`
					LEFT JOIN pelaporanlembaga AS C ON A.`int_id_pelaporan` = C.`int_id_pelaporan`
					WHERE C.`int_id_lembaga` = ".$int_id_lembaga." AND A.intProgress != '9'
					ORDER BY RAND()
					) AS E GROUP BY E.int_id_pelaporan ORDER BY E.dtTanggalPelaporan DESC, E.dtCreated DESC LIMIT ".$limit." OFFSET ".$start;
		$data = $this->db->query($query);
        return $data->result_array();
	}

	public function data_pelaporan($int_id_lembaga,$start,$limit){
		$query = "SELECT tp.*, tpi.txt_dir, tpi.int_source, ms.`txt_sumber`, mst.txt_status, mst.var_class stat_class, mk.var_class kat_class, mk.var_color
                    FROM $this->t_pelaporan tp
                    LEFT JOIN $this->t_pelaporan_img tpi ON tp.int_id_pelaporan = tpi.int_id_pelaporan
					LEFT JOIN $this->t_pelaporan_lembaga tpl ON tp.int_id_pelaporan = tpl.int_id_pelaporan
                    LEFT JOIN $this->m_sumber ms ON tp.int_id_sumber = ms.int_id_sumber
                    LEFT JOIN $this->m_kategori mk ON tp.int_id_kategori = mk.int_id_kategori
                    LEFT JOIN $this->m_status mst ON tp.int_status = mst.int_status
                    WHERE is_active = '1' AND tpl.int_id_lembaga = $int_id_lembaga
                    ORDER BY tp.dt_tanggal_pelaporan DESC, tp.dt_created DESC, tpi.int_source ASC LIMIT ".$limit." OFFSET ".$start;
		$data = $this->db->query($query);
        return $data->result_array();
	}
	
	public function ret_tujuan_pelaporan($int_id_pelaporan){
		$this->db->select("ml.txt_deskripsi, ml.int_id_lembaga");
		$this->db->from("{$this->t_pelaporan_lembaga} tpl");
		$this->db->join("{$this->m_lembaga} ml", "tpl.int_id_lembaga = ml.int_id_lembaga", "left");
		$this->db->where("tpl.int_id_pelaporan" , $int_id_pelaporan);
		$data = $this->db->get();
		return $data->result_array();
	}

	public function count_data_pelaporan($int_id_lembaga){
		$this->db->select("int_id_pelaporan");
		$this->db->from("$this->t_pelaporan_lembaga");
		$this->db->where("int_id_lembaga" , $int_id_lembaga);
        $data = $this->db->count_all_results();
		return $data;
	}

	public function nama_opd($int_id_lembaga){
		$this->db->select("txt_deskripsi, int_id_lembaga");
		$this->db->from("$this->m_lembaga");
		$this->db->where("int_id_lembaga" , $int_id_lembaga);
		$data = $this->db->get();
		return $data->result_array();
	}

	public function rekap_status_pelaporan_lembaga($int_id_lembaga,$int_status){
		$query = "SELECT COUNT(tpl.int_id_lembaga) AS jumlah
		FROM $this->t_pelaporan_lembaga tpl
		LEFT JOIN $this->t_pelaporan tp ON tpl.`int_id_pelaporan` = tp.`int_id_pelaporan`
		LEFT JOIN $this->m_lembaga ml ON tpl.`int_id_lembaga` = ml.`int_id_lembaga`
		WHERE tp.`int_status` = '".$int_status."' AND ml.`int_id_lembaga` = '".$int_id_lembaga."'";
		$data = $this->db->query($query);
        return $data->row('jumlah');
	}

	public function rekap_pelaporan(){
		$query = "SELECT * FROM (
				SELECT COUNT(tpl.int_id_lembaga) AS jumlah, ml.txt_deskripsi, ml.int_id_lembaga 
				FROM $this->m_lembaga ml
				LEFT JOIN $this->t_pelaporan_lembaga tpl ON tpl.`int_id_lembaga` = ml.`int_id_lembaga`
				LEFT JOIN $this->t_pelaporan tp ON tpl.`int_id_pelaporan` = tp.`int_id_pelaporan`
				GROUP BY ml.`int_id_lembaga`) A
				ORDER BY A.jumlah DESC, A.txt_deskripsi ASC";
		$data = $this->db->query($query);
        return $data->result_array();
	}
}