<div class="col-lg-3 order-1 order-lg-1" style="margin-bottom: 30px;">
	<aside class="widget-area">
		<!-- widget single item start -->
		<div class="card card-profile widget-item p-0">
			<div class="profile-banner">
				<figure class="profile-banner-small">
					<img src="<?=base_url()?>thm/images/laporlumajang.jpg" alt="">
				</figure>
				<div class="profile-desc text-center">
					<h6 class="author"><a href="profile.html"><?=$nama_opd?></a></h6>
				</div>
			</div>
		</div>
		<!-- widget single item start -->
		<div class="card widget-item">
			<h4 class="widget-title">Rekapitulasi Kinerja</h4>
			<div class="widget-body">
				<ul class="like-page-list-wrapper">
					<?=$rekap_pelaporan_opd?>
				</ul>
			</div>
			<h6 class="widget-footer" style="border-top: 1px solid #1c8567;margin-top: 30px;font-size: 12px;color: #1c8567;">
			*) 30 hari terakhir</h6>
		</div>
		<!-- widget single item end -->
	</aside>
</div>

<div class="col-lg-6 order-2 order-lg-2">
	<!-- post status start -->
	<?=$list_pelaporan;?>
	<?php echo $pagination;?>
	<!-- post status end -->
</div>