<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Opd extends MX_Controller {
	var $limit = "15";

	function __construct(){
        parent::__construct();
		
        //$this->kodeMenu = 'APP-HOME';
        $this->module   = 'opd';
		
		$this->load->model('Opd_model', 'model');
    }
	
	public function index(){
		$data_lembaga = $this->model->get_data_lembaga();
		$list_lembaga = '';
		foreach($data_lembaga as $row){
			$fb_url = '';
			if(!empty($row['txt_url'])){
				$fb_url = '<a href="'.$row['txt_url'].'" target="_blank" class="badge badge-primary"><i class="fab fa-facebook-f"> </i></a>';
			}
			$list_lembaga .= '<div class="row detil-list-opd">
								<a href="'.base_url().'opd/'.$row['int_id_lembaga'].'" class="col-8 col-sm-9 col-md-10 nama-opd">'.$row['txt_deskripsi'].'</a>
								<div class="col-4 col-sm-3 col-md-2 text-right post-settings-bar" style="padding-left: 0px !important;">
									'.$fb_url.'
									<a href="'.base_url().'opd/'.$row['int_id_lembaga'].'" class="badge badge-success">
										<i class="fas fa-info"> </i>
									</a>
								</div>
							</div>';
		}
		$data['list_lembaga'] = $list_lembaga;
		$this->render_view('index', $data, false, 'template');
	}
	
	function list($int_id_lembaga, $page = 1){
        $start = $this->limit * (abs($page)-1);
        $dt = array();
        $nama_opd = $this->model->nama_opd($int_id_lembaga);
		if($nama_opd){$data['nama_opd'] = $nama_opd[0]['txt_deskripsi'];}
		else{header('Location:'.base_url()); exit;}

        $data_pelaporan = $this->model->data_pelaporan($int_id_lembaga,$start,$this->limit);
		$count_data_pelaporan = $this->model->count_data_pelaporan($int_id_lembaga);
		
		$list_pelaporan = '';
		if(!empty($data_pelaporan)){
			foreach ($data_pelaporan as $row){
				$txt_img = '';//THEME_URL.'images/laporlumajang.jpg';
				if($row['txt_dir'] != ''){
					if($row['int_source']==1){
						$img_url = $row['txt_dir'];
					}else{
						$img_url = cdn_url().$row['txt_dir'];
					}
					$txt_img = '<div class="post-thumb-gallery">
								<figure class="post-thumb img-popup">
									<a href="'.$img_url.'">
										<img src="'.$img_url.'" alt="'.$row['txt_judul_pelaporan'].'">
									</a>
								</figure>
							</div>';
				}
				
				$tujuan = '-';
				$tujuan_pelaporan = $this->model->ret_tujuan_pelaporan($row['int_id_pelaporan']);
				if(!empty($tujuan_pelaporan)){
					$tujuan = '<i class="far fa-building" style="margin-right:5px;"> </i>';
					foreach($tujuan_pelaporan as $rowTujuan){
						$tujuan .= '<a href="'.base_url().'opd/'.$rowTujuan['int_id_lembaga'].'">'.$rowTujuan['txt_deskripsi'].'</a>,';
					}
				}
				
				$nama_pelapor = isset($row['txt_nama_pelapor'])? $row['txt_nama_pelapor'] : 'Member Grup Facebook';
				$var_color = isset($row['var_color'])? $row['var_color'] : '#6c757d';
				$kat_class = isset($row['kat_class'])? $row['kat_class'] : 'fas fa-folder';
	
				$list_pelaporan .= '<div class="card status-'.$row['int_id_kategori'].'">
							<div class="post-title d-flex align-items-center">
								<!-- profile picture end -->
								<div class="profile-thumb">
									<figure class="profile-thumb-middle lap-jenis-'.$row['int_id_kategori'].'" style="background:'.$var_color.'">
									<i class="'.$kat_class.'"></i>
									</figure>
								</div>
								<!-- profile picture end -->
					
								<div class="posted-author">
									<h6 class="author">'.$nama_pelapor.'</h6>
									<span class="post-time">'.date('d F Y H:i:s', strtotime($row['dt_tanggal_pelaporan'])).'</span>
									
								</div>
								<div class="post-settings-bar">
									<a href="'.$row['txt_url'].'" target="_blank" class="badge badge-primary">
										<i class="fab fa-facebook-f"> </i>
									</a>
									<a href="'.base_url().'detil/'.$row['var_kode_pelaporan'].'" class="badge badge-success">
										<i class="fas fa-info"> </i>
									</a>
								</div>
							</div>
							<!-- post title start -->
							<div class="post-content">
								<p class="post-desc">
								<b><a href="'.base_url().'detil/'.$row['var_kode_pelaporan'].'" style="color:#000">'.ucwords($row['txt_judul_pelaporan']).'</a></b>
								<span class="badge badge-pill badge-'.$row['stat_class'].'">'.$row['txt_status'].'</span><br>
								'.strip_tags($row['txt_detil_pelaporan']).'
								</p>
								'.$txt_img.'
								<div class="post-meta">
									<span class="post-meta-like">
										'.substr($tujuan, 0, -1).'
									</span>
								</div>
							</div>
						</div>';
			}
		}else{
			$html .= '<div class="col-md-12 col-12">
					<div class="card h-100">
						<div class="single-post post-style-1">
							<div class="blog-image"><a href="'.base_url().'"><br><br><br>Data pelaporan tidak ditemukan</a></div>
						</div>
					</div>
				</div>';
		}
		$data['list_pelaporan'] = $list_pelaporan;
		$data["pagination"] = $this->_build_pagination($page,$count_data_pelaporan,$this->limit,base_url()."opd/".$int_id_lembaga."/");
		$data['rekap_pelaporan_opd'] = $this->rekap_pelaporan_opd($int_id_lembaga);
		$this->render_view('list', $data, false, 'template');
	}
	
	private function rekap_pelaporan_opd($int_id_lembaga){	
		$report_opdAllNull = $this->model->rekap_status_pelaporan_lembaga($int_id_lembaga,'0');
		$report_opdAllOne = $this->model->rekap_status_pelaporan_lembaga($int_id_lembaga,'1');
		$report_opdAllTwo = $this->model->rekap_status_pelaporan_lembaga($int_id_lembaga,'2');
		$report_opdAllThree = $this->model->rekap_status_pelaporan_lembaga($int_id_lembaga,'3');
		
		//print_r($report_opdAllNull);
		$html = '';
		$jumlah_pelaporan = $report_opdAllNull + $report_opdAllOne + $report_opdAllTwo + $report_opdAllThree;
		if($jumlah_pelaporan != 0){
		$rasioNull = round(($report_opdAllNull/$jumlah_pelaporan*100),2);
		$rasioOne = round(($report_opdAllOne/$jumlah_pelaporan*100),2);
		$rasioTwo = round(($report_opdAllTwo/$jumlah_pelaporan*100),2);
		$rasioThree = round(($report_opdAllThree/$jumlah_pelaporan*100),2);
		$rasioSuccess = round((($report_opdAllTwo + $report_opdAllThree)/$jumlah_pelaporan*100),2);
		}
		$html .= '<table class="table" style="margin-bottom:0px !important">
				  <thead>
					<tr class="table-active">
					  <th scope="col">Jumlah Pelaporan</th>
					  <th scope="col" class="text-right">'.$jumlah_pelaporan.'</th>
					</tr>
				  </thead>
				  <tbody>
					<tr class="table-success">
					  <td>- Sudah Diselesaikan</td>
					  <td class="text-right">'.$report_opdAllThree.'</td>
					</tr>
					<tr class="table-info">
					  <td>- Dalam Perencanaan</td>
					  <td class="text-right">'.$report_opdAllTwo.'</td>
					</tr>
					<tr class="table-warning">
					  <td>- Proses Penanganan</td>
					  <td class="text-right">'.$report_opdAllOne.'</td>
					</tr>
					<tr class="table-danger">
					  <td>- Belum Ditanggapi</td>
					  <td class="text-right">'.$report_opdAllNull.'</td>
					</tr>
				  </tbody>
				</table>';
		if($jumlah_pelaporan != 0){
		$html .= '<div class="progress" style="margin-bottom:10px !important">
				  <div class="progress-bar bg-danger" role="progressbar" style="width:'.$rasioNull.'%" aria-valuenow="'.$rasioNull.'" aria-valuemin="0" aria-valuemax="100">'.$report_opdAllNull.'</div>
				  <div class="progress-bar bg-warning" role="progressbar" style="width: '.$rasioOne.'%" aria-valuenow="'.$rasioOne.'" aria-valuemin="0" aria-valuemax="100">'.$report_opdAllOne.'</div>
				  <div class="progress-bar" role="progressbar" style="width: '.$rasioTwo.'%" aria-valuenow="'.$rasioTwo.'" aria-valuemin="0" aria-valuemax="100">'.$report_opdAllTwo.'</div>
				  <div class="progress-bar bg-success" role="progressbar" style="width: '.$rasioThree.'%" aria-valuenow="'.$rasioThree.'" aria-valuemin="0" aria-valuemax="100">'.$report_opdAllThree.'</div>
				</div>';
		}
			
		if($jumlah_pelaporan != 0){
			if($rasioSuccess < 50){ $bg = 'danger';}
			else if($rasioSuccess < 75){ $bg = 'warning';}
			else{ $bg = 'success';}
			$html .= '<div class=""><b>Rasio Penyelesaian :</b></div>';
			$html .= '<div class="progress">
				  <div class="progress-bar progress-bar-striped bg-'.$bg.'" role="progressbar" style="width:'.$rasioSuccess.'%" aria-valuenow="'.$rasioSuccess.'" aria-valuemin="0" aria-valuemax="100">'.$rasioSuccess.'%</div>
				</div>';
		}
		return $html;
	}

	function rekapitulasi(){
		$data_rekapitulasi = $this->model->rekap_pelaporan();
		$rekap_lembaga = '';
		foreach($data_rekapitulasi as $row){
			$report_opdAllNull = $this->model->rekap_status_pelaporan_lembaga($row['int_id_lembaga'],'0');
			$report_opdAllOne = $this->model->rekap_status_pelaporan_lembaga($row['int_id_lembaga'],'1');
			$report_opdAllTwo = $this->model->rekap_status_pelaporan_lembaga($row['int_id_lembaga'],'2');
			$report_opdAllThree = $this->model->rekap_status_pelaporan_lembaga($row['int_id_lembaga'],'3');

			if($row['jumlah'] != 0){
				$rasioNull = round(($report_opdAllNull/$row['jumlah']*100),2);
				$rasioOne = round(($report_opdAllOne/$row['jumlah']*100),2);
				$rasioTwo = round(($report_opdAllTwo/$row['jumlah']*100),2);
				$rasioThree = round(($report_opdAllThree/$row['jumlah']*100),2);
				$rasioSuccess = round((($report_opdAllTwo + $report_opdAllThree)/$row['jumlah']*100),2);
				$dataBar = '<div class="progress" style="margin-bottom:10px !important">
				  <div class="progress-bar bg-danger" role="progressbar" style="width:'.$rasioNull.'%" aria-valuenow="'.$rasioNull.'" aria-valuemin="0" aria-valuemax="100">'.$report_opdAllNull.'</div>
				  <div class="progress-bar bg-warning" role="progressbar" style="width: '.$rasioOne.'%" aria-valuenow="'.$rasioOne.'" aria-valuemin="0" aria-valuemax="100">'.$report_opdAllOne.'</div>
				  <div class="progress-bar" role="progressbar" style="width: '.$rasioTwo.'%" aria-valuenow="'.$rasioTwo.'" aria-valuemin="0" aria-valuemax="100">'.$report_opdAllTwo.'</div>
				  <div class="progress-bar bg-success" role="progressbar" style="width: '.$rasioThree.'%" aria-valuenow="'.$rasioThree.'" aria-valuemin="0" aria-valuemax="100">'.$report_opdAllThree.'</div>
				</div>';

				if($rasioSuccess < 25){ $bg = 'danger';}
				else if($rasioSuccess < 50){ $bg = 'danger';}
				else if($rasioSuccess < 75){ $bg = 'warning';}
				else{ $bg = 'success';}
				$barRasio = '<div class="">Rasio Penyelesaian : <span class="badge badge-'.$bg.'">'.$rasioSuccess.'%</span></div>';

			}else{

				$dataBar = '<div class="progress">
					  <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
					</div>';
				$barRasio = '';
			}

			$rekap_lembaga .= '<div class="row detil-list-opd">
								<a href="'.base_url().'opd/'.$row['int_id_lembaga'].'" class="col-8 col-sm-9 col-md-10 nama-opd"><b>'.$row['txt_deskripsi'].'</b></a>
								<div class="col-4 col-sm-3 col-md-2 text-right" style="padding-left: 0px !important;">
									<b>'.$row['jumlah'].'</b> Pelaporan
								</div>
								<div class="col-12">'.$dataBar.'</div>
								<div class="col-12">'.$barRasio.'</div>
							</div>';
		}

		$data['rekap_lembaga'] = $rekap_lembaga;
		$this->render_view('rekapitulasi', $data, false, 'template');
    }
}
