<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['opd']['get']                    = 'opd/index';        //List Semua OPD
$route['opd/rekapitulasi']['get']       = 'opd/rekapitulasi';        //Rekap Semua OPD
$route['opd/([0-9]+)']['get']     = 'opd/list/$1';      //List Pelaporan OPD $1
$route['opd/([0-9]+)/([0-9]+)']['get']	= 'opd/list/$1/$2';   //List Pelaporan OPD $1 page $2
